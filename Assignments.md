# Assignments for this course.

## Instructions

1. Each student is assigned to some TA. You need give read access to
   your TA and no one else so that he/she can clone your repo and
   evaluate it. You can find bitbucket usernames of all TAs in
   [List of TA][ta].

2. For each assignment, please make a set of small commits instead of
   one single commit. Each logical stuff you do can be in a separate
   commit.  The commit messages act as a documentation on history of
   the project and hence having good logical commit messages are
   important for the project.

3. If there is any clarifications/mistakes please use the issue tracker.

#### Repository Structure
```
<your repository>
    |
    |-- assignment1
    |       |-- <other files>
    |       |-- README.md
    |
    |--
    |
    |-- assignment5
    |       |-- <other files>
    |       |-- README.md
    |
    |--
```

### Grading policy (common to all assignments).

1. The maximum marks for each assignment will be 1.

2. 1 mark will usually be reserved for good commit messages in your
   git repositories. However, we will give this mark subject to
   getting the assignment correct. For example, if the assignement is
   to write a Unix like kernel and you create a repository for hello
   world which has exceptional commit message, you will not get those
   marks.

3. Showing a Demo is a must. The demo will be taken by your TA on the
   week after the last day.  You will only get half the marks if the
   demo is not shown. Suppose that you deserve 8/10 for a particular
   assignment according to the grading policy, but you failed to show
   the demo, then you get only 4 marks.

4. Your assignment should work on the machine that has been allocated
   to you. Besides, the TAs will use the same commands that were
   discussed in the class to compile your work.

5. For assignments on latex and HTML you are *not allowed* to use a
   WYSIWYG program and generate the final assignment.



## Assignment 1

**Deadline: 25th Jan 2015 (Sunday) midnight**


The goal of this assignment is to get you familiar with latex and some
of its features.

1. Create your resume.
    * Add basic information
    * Add a passport size photograph
    * Add your transcript in tabular format. Please follow the format
      that DOAA uses. The grades and subjects taken can be fictitious but
      the format should be that of DOAA.

2. Write an article on any *one* of the following
    * Define AP and GP and prove its summation formula.
    * Define the Harmonic number and Eulers integral representation of
    it.  <http://en.wikipedia.org/wiki/Harmonic_number>. Prove that
    the n-th Harmonic number H(n) is Order (log n). Since the function
    $f(x) = 1/x$ is a monotonically decreasing, the idea is to show
    that the integral from 0 to n of 1/x approximates the H(n) above
    and below. Compare the areas under and above the curve in the picture
	given in the wikipedia entry. Details are available
	at <https://math.dartmouth.edu/~m105f13/m105f13notes1.pdf> but
	you need to express it in your own words.

   In each case you need to give a reference to a book or url. This should
   use bibtex.

## Assignment 2

**Deadline: 1st Feb 2015 (Sunday) midnight**

Write a make file for building the latex documents that you did as part
of assignment 1. In particular, it should support the following targets.

1. `dvi` : This should build the dvi version of the document.
2. `pdf` : This should build the pdf version of the document.
3. `all` : Should build both the dvi and pdf version.
4. `clean` : should remove all intermediate files.

Make sure that your make file also takes care of running bibtex as
well.  to generate the references. The `clean` target should remove
all the intermediate files including the intermediate files produced
by the bibtex.

## Assignment 3

**Deadline: 8th Feb 2015 (Sunday) midnight **

Create a home page with the following contents.

1. The start up page should contain your name and address.

2. There should be links from you home page to a pdf version of your
   cv (that you created in Assignment 1)

3. There should be a photograph of you in the main page. You can use a
   dummy photograph instead but the main idea is to understand how
   images are added.

4. You should have a favourite icon for your website. This shows up in
   the title bar of the browser (for example the bitbucket site shows
   the picture of a blue bucket). I suggest using the iitk logo as the
   icon.

You should host your home page on bitbucket using the
[bitbucket pages] feature.


## Assignment 4

**Deadline 8th March 2015 (Sunday) midnight**

1. Using the banner command, write a shell script to display a big
   digital clock on the terminal that ticks every second. The output
   should show the time in HH:MM:SS format. For example 30seconds past
   3:45 in the afternoon should be displayed as follows

           #    #######    #    #       #######    #     #####    ###
          ##    #         # #   #    #  #         # #   #     #  #   #
         # #    #          #    #    #  #          #          # # #   #
           #     #####          #######  #####           #####  #  #  #
           #          #    #         #        #    #          # #   # #
           #    #     #   # #        #  #     #   # #   #     #  #   #
         #####   #####     #         #   #####     #     #####    ###


2. The marks of the students are given as a table in the followin
   format `Name | rollno | marks in exam1 | marks in exam 2 ...`
   i.e. There is one record per line and each column is separated by a
   `|` (pipe) character. Write an awk script that will take each such
   line and do the following.

	* Adds an extra column to each line which is the total marks obtained by the student
	* At the end of all the record add 4, extra lines which contains
	  the min, max, average and standard deviation for each exam and the
	  total.
	* The number of exams or the number of students can be arbitrary but every row
	  will have equal number of columns.

	Here is a sample input

        Piyush | 12345 |     5 |     5 |     4
        James  |   007 |     0 |     0 |     7
        Knuth  | 31415 |   100 |   100 |   100


	For which the output is


        Piyush | 12345 |     5 |     5 |     4 |     14
        James  |   007 |     0 |     0 |     7 |      7
        Knuth  | 31415 |   100 |   100 |   100 |    300
        max    |       |   100 |   100 |   100 |    300
        min    |       |     0 |     0 |     4 |      7
        mean   |       | 35.00 | 35.00 | 37.00 | 107.00
        sd     |       | 46.01 | 46.01 | 44.56 | 136.50



[bitbucket pages]: <https://confluence.atlassian.com/display/BITBUCKET/Publishing+a+Website+on+Bitbucket>
[ta]: <https://bitbucket.org/ppk-teach/tools/wiki/List%20of%20TA>
